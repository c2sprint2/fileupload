let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); 
let tableComponent = require('../js/components/searchLibrary/tableComponent'); 
let chai = require('chai');
let expect = chai.expect;

describe('tableComponent testcases', function () {
  before('before creating tableComponent', function() {
    this.tableComponentComp = TestUtils.renderIntoDocument(<tableComponent />);
   
    
 });

   it('renders a composite component', () => {
  let tableComponentComp = TestUtils.renderIntoDocument(<tableComponent />);
  expect(TestUtils.isCompositeComponent(tableComponentComp)).to.equal(false);
});


   it('does not render a react element', () => {
  let tableComponentComp = TestUtils.renderIntoDocument(<tableComponent />);
  expect(TestUtils.isElement(tableComponentComp)).to.equal(false);
});

it('does render a DOM Component', () => {
  let tableComponentComp = TestUtils.renderIntoDocument(<tableComponent />);
  expect(TestUtils.isDOMComponent(tableComponentComp)).to.equal(true);
});


it('Renderes component with TABLECOMPONENT tag', function() {
   
     this.tableComponentComp = TestUtils.renderIntoDocument(<tableComponent />);
    let renderedDOM = ReactDOM.findDOMNode(this.tableComponentComp);
    expect(renderedDOM.tagName).to.equal("TABLECOMPONENT");
   
  });

   });