let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); 
let toolBar = require('../js/components/browse/toolBar'); 
let chai = require('chai');
let expect = chai.expect;

describe('toolBar testcases', function () {
  before('before creating toolBar', function() {
    this.toolBarComp = TestUtils.renderIntoDocument(<toolBar />);
    
    
 });

  it('renders a composite component', () => {
  let toolBarComp = TestUtils.renderIntoDocument(<toolBar />);
  expect(TestUtils.isCompositeComponent(toolBarComp)).to.equal(false);
});

    it('does not render a react element', () => {
  let toolBarComp = TestUtils.renderIntoDocument(<toolBar />);
  expect(TestUtils.isElement(toolBarComp)).to.equal(false);
});

 it('does render a DOM Component', () => {
  let toolBarComp = TestUtils.renderIntoDocument(<toolBar />);
  expect(TestUtils.isDOMComponent(toolBarComp)).to.equal(true);
});


  });