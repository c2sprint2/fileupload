let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); //I like using the Test Utils, but you can just use the DOM API instead.
let SavedSearchComponent = require('../js/components/searchLibrary/SavedSearchComponent'); //my root-test lives in components/__tests__/, so this is how I require in my components.
let chai = require('chai');
let expect = chai.expect;
import Paginate from '../js/components/pagination/paging';
import TableComponent from '../js/components/searchLibrary/tableComponent';

describe('SavedSearchComponent testcases', function () {
  before('before creating SavedSearchComponent', function() {
  	var handleChange=function(){};
  	var onSelect=function(){};
    this.SavedSearchComponentComp = TestUtils.renderIntoDocument(<SavedSearchComponent handleChange={handleChange} onSelect={onSelect} rows={[{}]} pageDetails={{pageLimit:2}}/>);
});

 it('check rendered tagName', function() {
 	var handleChange=function(){};
  	var onSelect=function(){}; 
    let renderedDOM = ReactDOM.findDOMNode(this.SavedSearchComponentComp);
    expect(renderedDOM.tagName).to.equal("DIV");
});

  it('renders a composite component', () => {
  	var handleChange=function(){};
  	var onSelect=function(){};
	let SavedSearchComponentComp = TestUtils.renderIntoDocument(<SavedSearchComponent handleChange={handleChange} onSelect={onSelect} rows={[{}]} pageDetails={{pageLimit:2}}/>);
	expect(TestUtils.isCompositeComponent(SavedSearchComponentComp)).to.equal(true);
});

it('does not render a react element', () => {
  var handleChange=function(){};
  var onSelect=function(){};
  let SavedSearchComponentComp = TestUtils.renderIntoDocument(<SavedSearchComponent handleChange={handleChange} onSelect={onSelect} rows={[{}]} pageDetails={{pageLimit:2}}/>);
  expect(TestUtils.isElement(SavedSearchComponentComp)).to.equal(false);
});

it('does not render a DOMComponent', () => {
  var handleChange=function(){};
  var onSelect=function(){};
  let SavedSearchComponentComp = TestUtils.renderIntoDocument(<SavedSearchComponent handleChange={handleChange} onSelect={onSelect} rows={[{}]} pageDetails={{pageLimit:2}}/>);
  expect(TestUtils.isDOMComponent(SavedSearchComponentComp)).to.equal(false);
});

it('should be rendered with several button when records are passed in rows', () => {
  var handleChange=function(){};
  var onSelect=function(){};
  let SavedSearchComponentComp = TestUtils.renderIntoDocument(<SavedSearchComponent handleChange={handleChange} onSelect={onSelect} rows={[{}]} pageDetails={{pageLimit:2}}/>);
  var button = TestUtils.scryRenderedDOMComponentsWithTag(SavedSearchComponentComp, 'button');
  expect(button.length).to.equal(2);
});

it('should be rendered with several divs when records are passed in rows', () => {
  var handleChange=function(){};
  var onSelect=function(){};
  let SavedSearchComponentComp = TestUtils.renderIntoDocument(<SavedSearchComponent handleChange={handleChange} onSelect={onSelect} rows={[{},{}]} pageDetails={{pageLimit:2}}/>);
  var divs = TestUtils.scryRenderedDOMComponentsWithTag(SavedSearchComponentComp, 'div');
  expect(divs.length).to.equal(19);
});

it('should be rendered with 2 divs when no records are passed in rows', () => {
  var handleChange=function(){};
  var onSelect=function(){};
  let SavedSearchComponentComp = TestUtils.renderIntoDocument(<SavedSearchComponent handleChange={handleChange} onSelect={onSelect} rows={[]} pageDetails={{pageLimit:2}}/>);
  var divs = TestUtils.scryRenderedDOMComponentsWithTag(SavedSearchComponentComp, 'div');
  expect(divs.length).to.equal(2);
});

it('should be rendered with 0 buttons when no records are passed in rows', () => {
  var handleChange=function(){};
  var onSelect=function(){};
  let SavedSearchComponentComp = TestUtils.renderIntoDocument(<SavedSearchComponent handleChange={handleChange} onSelect={onSelect} rows={[]} pageDetails={{pageLimit:2}}/>);
  var buttons = TestUtils.scryRenderedDOMComponentsWithTag(SavedSearchComponentComp, 'button');
  expect(buttons.length).to.equal(0);
});

 it('should renders a div using shallow render', () => {
  var handleChange=function(){};
  var onSelect=function(){};
  var renderer = TestUtils.createRenderer();
  renderer.render(<SavedSearchComponent handleChange={handleChange} onSelect={onSelect} rows={[]} pageDetails={{pageLimit:2}}/>);
  let result = renderer.getRenderOutput();
  expect(result.type).to.equal('div');
});


});
