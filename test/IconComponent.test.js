let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); //I like using the Test Utils, but you can just use the DOM API instead.
let IconComponent = require('../js/components/checkJobStatus/IconComponent'); //my root-test lives in components/__tests__/, so this is how I require in my components.
let chai = require('chai');
let expect = chai.expect;

describe('IconComponent testcases', function () {
  before('before creating IconComponent', function() {
    this.IconComponentComp = TestUtils.renderIntoDocument(<IconComponent />);
    this.input = TestUtils.findRenderedDOMComponentWithTag(this.IconComponentComp, 'div');
});

 it('check rendered tagName', function() { 
    let renderedDOM = ReactDOM.findDOMNode(this.IconComponentComp);
    expect(renderedDOM.tagName).to.equal("DIV");
});

  it('renders a composite component', () => {
  let IconComponentComp = TestUtils.renderIntoDocument(<IconComponent  />);
  expect(TestUtils.isCompositeComponent(IconComponentComp)).to.equal(true);
});

it('does not render a react element', () => {
  let IconComponentComp = TestUtils.renderIntoDocument(<IconComponent />);
  expect(TestUtils.isElement(IconComponentComp)).to.equal(false);
});

it('does not render a DOMComponent', () => {
  let IconComponentComp = TestUtils.renderIntoDocument(<IconComponent />);
  expect(TestUtils.isDOMComponent(IconComponentComp)).to.equal(false);
});

it('should be rendered with single div', () => {
	  let IconComponentComp = TestUtils.renderIntoDocument(<IconComponent />);
	  var div = TestUtils.scryRenderedDOMComponentsWithTag(IconComponentComp, 'div');
	  expect(div.length).to.equal(1);
	});

it('should be rendered with single img tag', () => {
	  let IconComponentComp = TestUtils.renderIntoDocument(<IconComponent />);
	  var img = TestUtils.scryRenderedDOMComponentsWithTag(IconComponentComp, 'img');
	  expect(img.length).to.equal(1);
	});

});

