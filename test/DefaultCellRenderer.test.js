let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils');
let defaultCellRenderer = require('../js/components/folderpane/DefaultCellRenderer.js');
let chai = require('chai');
let expect = chai.expect;

describe('DefaultCellRenderer testcases ', function () {
  before('before creating DefaultCellRenderer ', function() {
    this.defaultCellRendererComp = TestUtils.renderIntoDocument(<defaultCellRenderer />);
});
it('renders a composite component', () => {
    var parent1=function(a,b){
        let fileName=a;
        let nodeRef=b;
    };
  let defaultCellRendererComp = TestUtils.renderIntoDocument(<defaultCellRenderer  parent={parent1("abc","ad")}/>);
  expect(TestUtils.isCompositeComponent(defaultCellRendererComp)).to.equal(false);
});
it('renders a DOM component', () => {
  let defaultCellRendererComp = TestUtils.renderIntoDocument(<defaultCellRenderer />);
  expect(TestUtils.isDOMComponent(defaultCellRendererComp)).to.equal(true);
});

it('does not render a react element', () => {
  let defaultCellRendererComp = TestUtils.renderIntoDocument(<defaultCellRenderer />);
  expect(TestUtils.isElement(defaultCellRendererComp)).to.equal(false);
});

it('Component Renders with the Correct DOM', function() {
   // this.defaultCellRendererComp = TestUtils.renderIntoDocument(<defaultCellRenderer />);
    let renderedDOM = ReactDOM.findDOMNode(this.defaultCellRendererComp);
    expect(renderedDOM.tagName).to.equal("DEFAULTCELLRENDERER");
    expect(renderedDOM.classList.length).to.equal(0);
    var children = renderedDOM.querySelectorAll('div');
    expect(children.length).to.equal(0);
});

it('should rendered with span tag', function() {
    this.defaultCellRendererComp = TestUtils.renderIntoDocument(<defaultCellRenderer />);
    let renderedDOM = ReactDOM.findDOMNode(this.defaultCellRendererComp);
    expect(renderedDOM.tagName).to.equal("DEFAULTCELLRENDERER");
});


});