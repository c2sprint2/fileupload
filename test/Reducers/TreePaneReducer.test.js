//let TreePaneConstants = require('../js/constants/TreePaneConstant');
import { GET_FOLDER, TOGGLE} from '../../js/constants/TreePaneConstant'
import TreePaneReducer from '../../js/reducers/TreePaneReducer'
//import marklogicMetadataAPI from '../../js/api/marklogicMetadataAPI'
import * as actions from '../../js/action/TreePaneAction'
import expect from 'expect'



describe('TreePane reducer', () => {

	let initilizeValues = [{

              items: '',
               expanded: true
}];

/*const treeConstants = {

								GET_FOLDER : 'GET_FOLDER',
								TOGGLE : 'TOGGLE_TEST'
					  };*/



	before('before creating ', function() {
		
    
    });      

 it('should handle initial state', () => {
  let ret = TreePaneReducer(undefined,{});
    expect(ret).toEqual(initilizeValues);
  });


 it('should handle GET_FOLDER action', () => {

  let nextState = TreePaneReducer({}, {
        type:GET_FOLDER,
        data:[
                { "id": "1", "name": "Apples" },
                { "id": "2", "name": "Pears" }
            ] 
          

    });

    expect(nextState).toEqual([{
            "items": [
                { "id": "1", "name": "Apples" },
                { "id": "2", "name": "Pears" }
            ],
            "expanded": true
          }]);

    });

 
   
  });

