//import metadataConstants from '../../js/constants/MetadataConstants';
import CheckJobStatusReducer from '../../js/reducers/CheckJobStatusReducers'
//import marklogicMetadataAPI from '../../js/api/marklogicMetadataAPI'
import * as actions from '../../js/action/CheckJobStatusAction'
import expect from 'expect'

describe('CheckJobStatus reducer', () => {

 const initilizeData = [{
      rows : [],
      columns : []
      
}];

before('before creating ', function() {
    
          });    

  it('should handle initial state', () => {
   
  let ret = CheckJobStatusReducer(undefined,{});
    expect(ret).toEqual(initilizeData);
  });

  it('should handle JOB_STATUS action', () => {

  let nextState = CheckJobStatusReducer({}, {
        type:JOB_STATUS,
        Data:{
            "rows": [
                { "id": "1", "name": "Apples" },
                { "id": "2", "name": "Pears" }
            ],
            "columns": [
                { "id": "3", "name": "Bananas" },
                { "id": "4", "name": "Mangos" },
                { "id": "5", "name": "Lemons" },
                { "id": "6", "name": "Apricots" }
            ]
          }

    });

    expect(nextState).toEqual({
            "rows": [
                { "id": "1", "name": "Apples" },
                { "id": "2", "name": "Pears" }
            ],
            "columns": [
                { "id": "3", "name": "Bananas" },
                { "id": "4", "name": "Mangos" },
                { "id": "5", "name": "Lemons" },
                { "id": "6", "name": "Apricots" }
            ]
          });

    });


});