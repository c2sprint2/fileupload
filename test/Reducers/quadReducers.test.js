import { SEND_TO_QUAD} from '../../js/constants/fileUploadConstants'
import quadReducer from '../../js/reducers/quad'
import expect from 'expect'

describe('quad reducer', () => {


before('before creating quadReducer', function() {
		
    
    });  

 it('should handle initial state', () => {
  let ret = quadReducer(undefined,{});
    expect(ret).toEqual([]);
  });       

 it('should handle SEND_TO_QUAD action', () => {

  let nextState = quadReducer([], {
        type:SEND_TO_QUAD,
        data:{
            "rows": [
                { "id": "1", "name": "Apples" },
                { "id": "2", "name": "Pears" }
            ],
            "columns": [
                { "id": "3", "name": "Bananas" },
                { "id": "4", "name": "Mangos" },
                { "id": "5", "name": "Lemons" },
                { "id": "6", "name": "Apricots" }
            ]
          }

    });

    expect(nextState).toEqual([{
            "rows": [
                { "id": "1", "name": "Apples" },
                { "id": "2", "name": "Pears" }
            ],
            "columns": [
                { "id": "3", "name": "Bananas" },
                { "id": "4", "name": "Mangos" },
                { "id": "5", "name": "Lemons" },
                { "id": "6", "name": "Apricots" }
            ]
          }]);

    });


});
