import savedSearchReducers from '../../js/reducers/savedSearchReducers'
import * as actions from '../../js/action/CheckJobStatusAction'
import expect from 'expect'

describe('savedSearch reducer', () => {

 const initilizeData = [{
       data:{
    "data":[]
  },
  "savedData":[],
  isChecked:false,
  enableDelete:false,
  isSavedSearch: false
      
}];

before('before creating ', function() {
    
          });    

  it('should handle initial state', () => {
   
  let ret = savedSearchReducers(undefined,{});
    expect(ret).toEqual(initilizeData);
  });

   });