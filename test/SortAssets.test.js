let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); 
let SortAssets = require('../js/components/common/SortAssets'); 
let chai = require('chai');
let expect = chai.expect;

describe('SortAssets testcases', function () {
  before('before creating SortAssets', function() {
  	var sortOptions=[];
    this.SortAssetsComp = TestUtils.renderIntoDocument(<SortAssets  sortOptions={sortOptions}/>);
   
    
 });

   it('renders a composite component', () => {
    var sortOptions=[];
  let SortAssetsComp = TestUtils.renderIntoDocument(<SortAssets  sortOptions={sortOptions} />);
  expect(TestUtils.isCompositeComponent(SortAssetsComp)).to.equal(true);
});


   it('does not render a react element', () => {
    var sortOptions=[];
  let SortAssetsComp = TestUtils.renderIntoDocument(<SortAssets  sortOptions={sortOptions} />);
  expect(TestUtils.isElement(SortAssetsComp)).to.equal(false);
});

it('does render a DOM Component', () => {
  var sortOptions=[];
  let SortAssetsComp = TestUtils.renderIntoDocument(<SortAssets  sortOptions={sortOptions} />);
  expect(TestUtils.isDOMComponent(SortAssetsComp)).to.equal(false);
});

it('should be rendered with single div', () => {
    var sortOptions=[];
    let SortAssetsComp = TestUtils.renderIntoDocument(<SortAssets  sortOptions={sortOptions} />);
    var div = TestUtils.scryRenderedDOMComponentsWithTag(SortAssetsComp, 'div');
    expect(div.length).to.equal(1);
  });

  it('should be rendered with single span', () => {
    var sortOptions=[];
    let SortAssetsComp = TestUtils.renderIntoDocument(<SortAssets  sortOptions={sortOptions} />);
    var span = TestUtils.scryRenderedDOMComponentsWithTag(SortAssetsComp, 'span');
    expect(span.length).to.equal(1);
  });

  it('Renderes Input tag with "sort-asset" class', function() {
    var sortOptions=[];
    this.SortAssetsComp = TestUtils.renderIntoDocument(<SortAssets  sortOptions={sortOptions} />); 
    let renderedDOM = ReactDOM.findDOMNode(this.SortAssetsComp);
    expect(renderedDOM.tagName).to.equal("DIV");
    expect(renderedDOM.classList.length).to.equal(1);
    expect(renderedDOM.classList[0]).to.equal("sort-asset");
  });

it('should renders a div using shallow render', () => {
  var sortOptions=[];
  var renderer = TestUtils.createRenderer();
  renderer.render(<SortAssets  sortOptions={sortOptions}/>);
  let result = renderer.getRenderOutput();
  expect(result.type).to.equal('div');
});

   });