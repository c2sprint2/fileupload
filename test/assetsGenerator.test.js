let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils'); //I like using the Test Utils, but you can just use the DOM API instead.
let AssetsGenerator = require('../js/components/browse/assetsGenerator'); //my root-test lives in components/__tests__/, so this is how I require in my components.
let chai = require('chai');
let expect = chai.expect;

describe('AssetsGenerator testcases', function () {
  before('before creating AssetsGenerator', function() {
  	var onSelect=function(){};
    this.AssetsGeneratorComp = TestUtils.renderIntoDocument(<AssetsGenerator assetsData={{}} onSelect={onSelect}/>);
  });

it('check rendered tagName', function() {
    var onSelect=function(){}; 
	this.AssetsGeneratorComp = TestUtils.renderIntoDocument(<AssetsGenerator assetsData={[]} onSelect={onSelect}/>);
    let renderedDOM = ReactDOM.findDOMNode(this.AssetsGeneratorComp);
    expect(renderedDOM.tagName).to.equal("DIV");
});

it('should be rendered with several divs when no record is passed', () => {
  var onSelect=function(){}; 
  let AssetsGeneratorComp = TestUtils.renderIntoDocument(<AssetsGenerator assetsData={[]} onSelect={onSelect}/>);
  var divs = TestUtils.scryRenderedDOMComponentsWithTag(AssetsGeneratorComp, 'div');
  expect(divs.length).to.equal(2);
});

it('renders a composite component', () => {
  var onSelect=function(){}; 
  let AssetsGeneratorComp = TestUtils.renderIntoDocument(<AssetsGenerator assetsData={[]} onSelect={onSelect}/>);
  expect(TestUtils.isCompositeComponent(AssetsGeneratorComp)).to.equal(true);
});

it('does not render a react element', () => {
  var onSelect=function(){}; 
  let AssetsGeneratorComp = TestUtils.renderIntoDocument(<AssetsGenerator assetsData={[]} onSelect={onSelect}/>);
  expect(TestUtils.isElement(AssetsGeneratorComp)).to.equal(false);
});

 it('should renders a div using shallow render', () => {
  var onSelect=function(){};
  var renderer = TestUtils.createRenderer();
  renderer.render(<AssetsGenerator assetsData={[]} onSelect={onSelect}/>);
  let result = renderer.getRenderOutput();
  expect(result.type).to.equal('div');
});


});
