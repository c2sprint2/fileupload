let React = require('react');
let ReactDOM = require('react-dom');
let TestUtils = require('react-addons-test-utils');
let TreePane = require('../js/components/folderpane/TreePane.js');
let chai = require('chai');
let expect = chai.expect;

describe('TreePane testcases ', function () {
  before('before creating TreePane ', function() {
    var toggle1=function(){};
    this.treePaneComp = TestUtils.renderIntoDocument(<TreePane model={{items:[]}} toggle={toggle1}/>);
});

it('renders a composite component', () => {
  var toggle1=function(){};
  let treePaneComp = TestUtils.renderIntoDocument(<TreePane model={{items:[]}} toggle={toggle1}/>);
  expect(TestUtils.isCompositeComponent(treePaneComp)).to.equal(true);
});

  it('should rendered with span tag', function() {
    var toggle1=function(){};
    this.treePaneComp = TestUtils.renderIntoDocument(<TreePane model={{items:[]}} toggle={toggle1}/>);
    let renderedDOM = ReactDOM.findDOMNode(this.treePaneComp);
    expect(renderedDOM.tagName).to.equal("DIV");
});

it('should be rendered with several divs', () => {
  var toggle1=function(){};
  let treePaneComp = TestUtils.renderIntoDocument(<TreePane model={{items:[]}} toggle={toggle1}/>);
  var divs = TestUtils.scryRenderedDOMComponentsWithTag(treePaneComp, 'div');
  expect(divs.length).to.equal(2);
});

it('renders a DOMComponent component', () => {
  var toggle1=function(){};
  let treePaneComp = TestUtils.renderIntoDocument(<TreePane model={{items:[]}} toggle={toggle1}/>);
  expect(TestUtils.isDOMComponent(treePaneComp)).to.equal(false);
});

 it('Renderes div tag with "Node" class', function() {
    var toggle1=function(){};
    this.treePaneComp = TestUtils.renderIntoDocument(<TreePane model={{items:[]}} toggle={toggle1}/>);
    var divs = TestUtils.scryRenderedDOMComponentsWithTag(this.treePaneComp, 'span');
    let renderedDOM = ReactDOM.findDOMNode(this.treePaneComp);
    expect(renderedDOM.tagName).to.equal("DIV");
    expect(renderedDOM.classList.length).to.equal(1);
    expect(renderedDOM.classList[0]).to.equal("Node");

});


});
