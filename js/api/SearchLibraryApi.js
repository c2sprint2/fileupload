const request = require('superagent');
const  Promise = require('bluebird');
const localforage = require('localforage');
import serviceUrls from '../constants/service';
import {format} from '../utils/stringManipulation';

var filterUrl = serviceUrls.searchBasedAll;

export default {
     autoComplete_Data(text) {

     	const value = text;

     	const Aflerco_Search_Url = serviceUrls.Alfresco_Search_Url;

     return Promise.promisifyAll(
           request
          .get(Aflerco_Search_Url)
          .query({ t: value})
          .auth('admin', 'admin')
	        .query({ limit: '4' })
          .on('progress', function(e){
          console.log("auto-suggest value is fetched");
         })
    	)
  },

       getAssertsData(text, filter,index,limit, sortOption='cm:created|false') { 
          if (filter !== '' && filter !== undefined) {
              filterUrl = filter;
          }
        console.log(filterUrl);
        let url = format(filterUrl, [text, sortOption]);
        url+="&maxResults="+limit+"&startIndex="+index;
        console.log(url);
     return Promise.promisifyAll(
           request
          .get(url)
          .auth('admin', 'admin')

      )
/*    localforage.setItem('key11',null);
      localforage.setItem('savedSearch', []).then(function () {
       var a =  localforage.getItem('savedSearch');
       console.log("sdsdd"+a);
       localforage.getItem('savedSearch', function(err, readValue) {
            console.log('Read value is ', readValue);
            let savedSearch = readValue;
            if(Array.isArray(savedSearch)){
              savedSearch.push({"searchTerm":"polynomial", "filter":"Difficult"});
              localforage.setItem('savedSearch',savedSearch);
            }
          });
    })*/
  },

  saveSearchValue(text) {
      return localforage.getItem('savedSearch', function(err, readValue) {
            let savedSearch = readValue;
            if(readValue == null || readValue.length==0){
              savedSearch =[];
              //i=1;
            }
            if(Array.isArray(savedSearch)){
              if(savedSearch.length){
              //var i = savedSearch.length + 1;
              }
              var AlreadyExists =false;
              for(var j=0;j<savedSearch.length;j++){
                if(savedSearch[j].searchterm==text){
                  AlreadyExists = true;
                }
              }
              if(AlreadyExists){
                alert("Value already exists in Saved Search");
              }else{
              var randomId = Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
              savedSearch.unshift({"searchterm": text,"id":randomId, "filter":"Difficult","isChecked":false});
              localforage.setItem('savedSearch',savedSearch);
               alert("Search value saved successfully");
            }
            }
          });

/*       autoComplete_Data() {
    return Promise.promisifyAll(
        request
       .get('http://localhost:3000/autoCompleteDataSugg')
      )
  }*/
  },

    getSearchValue(text) {
       localforage.getItem('savedSearch', function(err, readValue) {
            console.log('Read value is ', readValue);
          });

  }
}