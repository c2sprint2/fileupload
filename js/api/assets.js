const request = require('superagent');
const  Promise = require('bluebird');
let join = Promise.join;
import serviceUrls from '../constants/service';
import {format} from '../utils/stringManipulation';

let filterString = '';

export default {
   get_assets(nodeRef, index, limit, filter, sortValue='cm:created|false') {
    let imageUrls = [];

    if (filter !== '' && filter !== undefined) {
        filterString = filter;
    }
   
    let folderListUrl = format(serviceUrls.getAssets, [nodeRef, filterString,sortValue]);
    folderListUrl+="&maxResults="+limit+"&startIndex="+index;
    console.log("folderListUrl : "+folderListUrl);
    return Promise.promisifyAll(
          request
          .get(folderListUrl)
          .auth('admin', 'admin')
        );
  },

  //Waiting for clarification for displaying image authentication.
  getThumnailImages (nodeRefs) {
    let imageUrls = [];
    let i = 0;
    let assetsData = [];

       for (let obj in nodeRefs.objects) {
          if (nodeRefs.objects.hasOwnProperty(obj)) {
            let node = nodeRefs.objects[obj];
            let refValue = node.object.properties["alfcmis:nodeRef"].value;
            let temp = refValue.split('/');
            let refId = temp[temp.length -1];
            let imageUrl = format(image, [refId]);
            let temp1;
            imageUrls.push(imageUrl);

            if (node.object.properties["cmis:contentStreamMimeType"] === undefined) {
                temp1 = ''
            }
            else {
                temp1 = node.object.properties["cmis:contentStreamMimeType"].value;
            }

            assetsData.push({
                desc: node.object.properties["cmis:description"].value,
                type: temp1,
                url: imageUrl,
                content: '',
                id: i
            });
            i = i + 1;
          }
        }

        nodeRefs.assetsData = assetsData;

        return nodeRefs;

        // return Promise.promisifyAll(join(
        //         request.get(imageUrls[0]),
        //         request.get(imageUrls[1]),
        //         request.get(imageUrls[2]),
        //         request.get(imageUrls[3]),
        //         function (res1, res2, res3, res4) {
        //             // assetsData[0].content = btoa(res1.text);
        //             // assetsData[1].content = btoa(res2.text);
        //             // assetsData[2].content = btoa(res3.text);
        //             // assetsData[3].content = btoa(res4.text);
        //             nodeRefs.assetsData = assetsData;
        //             console.log(nodeRefs);
        //             return nodeRefs;
        //         }

        //     )
        // )
    }
}