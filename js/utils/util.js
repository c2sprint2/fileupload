module.exports = {
    getCurrentValues: (dataArray) => {
      if (dataArray.size > 1) {
        let latestItem = dataArray.size-1;
        return dataArray.get(latestItem);
      }

      return [];
    }
}
