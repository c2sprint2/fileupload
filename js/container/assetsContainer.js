import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {fetchingAssets, selectedRecord} from '../action/assets';
import {getSearchProductItems,saveSearchValues} from '../action/SearchLibraryAction';
import assetsGenerator from '../components/browse/assetsGenerator';
import serviceUrls from '../constants/service';
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../constants/paginationConstants';

const getSelectedValues = (dataArray) => {
  if (dataArray.size > 1) {
    let latestItem = dataArray.size-1;
    return dataArray.get(latestItem);
  }

  return [];
}

const mapStateToProps = (state) => {
  let data = getSelectedValues(state.assets);
  let selectedRecord = getSelectedValues(state.quad);
  let temp = null;

  if (data.length !== 0) {
    temp = JSON.parse(JSON.stringify(data.items));
  }
  return {
   assetsData: temp,
   pageDetails: data,
   selectedRecord: selectedRecord,
   saveSearchVisibility: data.showSaveSearch
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  	 onSelect: function onSelect(page, event) {
      event.preventDefault();
      let nodeRef;
      let filterUrlsForSearch = {
        0:serviceUrls.searchBasedAll,
        1:serviceUrls.searchBasedImage,
        2:serviceUrls.searchBasedVideo,
        3:serviceUrls.searchBasedAudio
      };
      let viewName;
      if(document.querySelector('.dropdown-display span i').className=="fa fa-list"){
        viewName = "list-view";
      }else{
        viewName = "grid-view";
      }

      nodeRef = document.querySelector('.filter-container .tree-node-selected');
      if (nodeRef) {
        let id = nodeRef.id;
        let maxItems = parseInt(document.querySelector('#itemPerPageSelectBox').value);
        dispatch(fetchingAssets(id, page,maxItems,'','',viewName));
      }
      },

      onChange:function(event){
        event.preventDefault();
        let viewName;
      if(document.querySelector('.dropdown-display span i').className=="fa fa-list"){
        viewName = "list-view";
      }else{
        viewName = "grid-view";
      }
        let nodeRef;
        nodeRef = document.querySelector('.filter-container .tree-node-selected');
        if (nodeRef) {
        let id = nodeRef.id;
        dispatch(fetchingAssets(id, DEFAULT_PAGE_NO,parseInt(event.target.value),'','',viewName));
        }
      },

      setSelectedItem: function (record) {
        dispatch(selectedRecord(record));
      },

      saveSearch:function(event){
        event.preventDefault();
        if(document.querySelector('.react-autosuggest__input').value){
          var SearchValue = document.querySelector('.react-autosuggest__input').value;
          dispatch(saveSearchValues(SearchValue));
        }
      },

      onSort: function(sortValue){
        let nodeRef;
        nodeRef = document.querySelector('.filter-container .tree-node-selected');
        if (nodeRef) {
        let id = nodeRef.id;
        let maxItems = parseInt(document.querySelector('#itemPerPageSelectBox').value);
        dispatch(fetchingAssets(id, DEFAULT_PAGE_NO,maxItems,'', sortValue));
        }
  },

    changeView:function(viewName){
        var maxItems;
        if(viewName === "list-view"){
          maxItems = 25;
        }else{
          maxItems = 9;
        }
        let nodeRef;
        nodeRef = document.querySelector('.filter-container .tree-node-selected');
        if (nodeRef) {
        let id = nodeRef.id;
        dispatch(fetchingAssets(id, DEFAULT_PAGE_NO,maxItems,'','',viewName));
        }
      }
}
}

const assetsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(assetsGenerator)

export default assetsContainer;