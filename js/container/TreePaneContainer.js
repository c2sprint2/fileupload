import React from 'react';
import { connect } from 'react-redux';
import {getFolders, toggle} from '../action/TreePaneAction';
import {fetchingAssets} from '../action/assets';
import TreePaneRenderer from '../components/folderpane/FolderPane';
import {highlightChildren, getFirstName, recentlySelectedChild} from '../components/common/browseAssetUtil';
import TreeNodeUtil from '../components/folderpane/TreeNodeUtil';
import {DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS} from '../constants/paginationConstants';
const getSelectedValues = (dataArray) => {
  if (dataArray.length > 0) {
    return dataArray[dataArray.length-1];
  }
  return [];
}

const mapStateToProps = (state) => { 
  let data = getSelectedValues(state.TreePaneReducers)
  return {
   model: data,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
     componentWillMount: function () { 
       dispatch(getFolders());
      },
     toggle:function(model,foldername, nodeRef){ 
      console.log(" Folder Name : "+ foldername +" ... "+" Node Ref : "+nodeRef);
      recentlySelectedChild(nodeRef);
      dispatch(fetchingAssets(nodeRef, DEFAULT_PAGE_NO,DEFAULT_MAX_RESULTS,''));
      }
  }
}

const treePaneContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TreePaneRenderer)

export default treePaneContainer;