import React, { Component, PropTypes } from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import SearchAssetsContainer from '../../container/searchAssetsContainer';
import {injectIntl, intlShape} from 'react-intl';
import SavedSearchContainer from '../../container/SavedSearchContainer';
import {messages} from './assetFiltersDefaultMessages';

class searchAssetsFilter extends Component {
   static PropTypes = {
        intl: intlShape.isRequired
    }

  constructor(props) {
    super(props);
       this.state = {
        selectedTab: this.props.selectedIndex // Items per page
      }
    this.tabHandleSelect = props.tabHandleSelect.bind(this);
  }
  componentWillReceiveProps(nextProps){
     this.setState({
        selectedTab: nextProps.selectedIndex
     });
  }

  render() {
    const {formatMessage} = this.props.intl;
    return (
      <div className="pe-assetFilters">
         <Tabs  id="filterAssets" onSelect={this.tabHandleSelect} selectedIndex={this.state.selectedTab}>
          <TabList>
            <Tab selected>{formatMessage(messages.All_Heading)}</Tab>
            <Tab>{formatMessage(messages.Image_Heading)}</Tab>
            <Tab>{formatMessage(messages.Video_Heading)}</Tab>
            <Tab>{formatMessage(messages.Audio_Heading)}</Tab>
            <Tab>{formatMessage(messages.Saved_Search_Heading)}</Tab>
          </TabList>
          <TabPanel>
            <SearchAssetsContainer filter="all"/>
          </TabPanel>
           <TabPanel>
            <SearchAssetsContainer filter="image"/>
          </TabPanel>
          <TabPanel>
            <SearchAssetsContainer filter="video"/>
          </TabPanel>
          <TabPanel>
            <SearchAssetsContainer filter="audio"/>
           </TabPanel>
            <TabPanel>
              <SavedSearchContainer/>
           </TabPanel>
        </Tabs>
    </div>
    )
  }
}

export default injectIntl(searchAssetsFilter);