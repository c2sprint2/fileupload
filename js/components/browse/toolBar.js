import React from 'react';
import ReactDOM from 'react-dom';
import Dropdown from '../common/Dropdown'

class toolBar extends React.Component  {
  
  constructor(props) { 
    super(props)
  }
 
  handleChange (element) { 
    this.props.changeView(element);
  }
 
  render () { 
    let views = [{
      name: "fa fa-list",
      value: "list-view"
    }, {
      name: "fa fa-th",
      value: "grid-view"
    }];
    return (
        <div className="dropdownWrapper">
            <Dropdown 
                  list={views} 
                  selected={views[1]} 
                  onChange={this.handleChange.bind(this)}/>
      </div>
    )
  }
}
module.exports = toolBar;
