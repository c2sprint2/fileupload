import React from 'react';
import SelectBox from '../common/SelectBox';

class ItemsPerPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      //pageLimit:4 // Items per page
      }
    };

  render() {
    var itemsPerPageListData=[
      {
         "value":"25",
         "text":"25"
      },
      {
         "value":"50",
         "text":"50"
      },
      {
         "value":"75",
         "text":"75"
      }
   ];

    var itemsPerPageThumbData=[
      {
         "value":"9",
         "text":"9"
      },
      {
         "value":"18",
         "text":"18"
      },
      {
         "value":"27",
         "text":"27"
      }
   ];

   var itemsPerPageData;

   if(this.props.pageView=='grid-view'){
    itemsPerPageData = itemsPerPageThumbData;
  }else{
   itemsPerPageData = itemsPerPageListData;
  }

    var itemsPerPageDetails = this.props.itemsPerPageDetails;
    var TotalCount = itemsPerPageDetails.numberFound;
    var SearchValue;
    if(this.props.itemsPerPageDetails.SearchValue){
      SearchValue = " for "+this.props.itemsPerPageDetails.SearchValue;
    }

    return (
      <div>
        <div className='per-page-container'>
          Display&nbsp;
          <SelectBox id='itemPerPageSelectBox' value={this.props.itemsPerPageDetails.displayItemCount} class='itemPerPageSelectBox' options={itemsPerPageData} onChange={this.props.onChange}/>
          <span> of {TotalCount} results {SearchValue}</span> 
        </div>
      </div>
    );
  }
}


module.exports = ItemsPerPage;