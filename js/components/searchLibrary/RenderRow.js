import React from 'react';
import CheckboxComponent from './CheckboxComponent.js';
import { checkBoxHandler } from '../../action/savedSearchAction.js';

class RenderRow extends React.Component{

	constructor(props){
        super(props);
        this.checkChangeHandler = this.checkChangeHandler.bind(this);
    }

	
	checkChangeHandler(obj){
		if(this.props.CheckboxHandler){
            this.props.CheckboxHandler(obj);
		}
	}

displayJson(jsonObj,self){
	var obj = jsonObj;
	console.log('obj: '+obj);
	var itemArr = [];
	console.log(itemArr);
	if (typeof jsonObj.map == 'function') {
		jsonObj.map(function(item,index){
			var id = item.id;
			var searchTerm = item.searchterm;
			var filter = item.filter;
			var isChecked = item.isChecked;
			itemArr.push(<div className='row-inner'>
					<div className='col-14'>
					<span className='savedSearchChkbox'>
					<CheckboxComponent name={searchTerm} val={searchTerm} id={id} checked={item.isChecked} onChangeHandler={self.checkChangeHandler.bind(this)}/>
					</span>
					<span className='savedSearchTerm'>{searchTerm}</span>
					</div>
					<div className='col-15'>{filter}</div>
				</div>);
			
		});
	}

    return itemArr;
}


render(){
	var self = this;
	var rows = this.props.rows;
	console.log('rows: '+rows);
	var test;
	if(rows !== undefined){
		test = this.displayJson(rows,self);
	}
	if(test==null){
		var test = 'test is null';
	}else{
		console.log('test: '+test);
	}
	
	return(
		<div>
			{test}
		</div>
		)
}
}
export default RenderRow;