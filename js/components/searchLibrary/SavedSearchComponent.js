import React, { Component, PropTypes } from 'react';
import { Link, browserHistory } from 'react-router';
import TableComponent from './tableComponent';
import AssetFiltersContainer from '../../container/AssetFiltersContainer';
import Paginate from '../pagination/paging';

class SavedSearchComponent extends Component {

    constructor(props) {
        super(props);
        this.displayName = 'ReviewAssetMedataComp';
        this.componentWillMount = props.componentWillMount;
        this.handleChange = props.handleChange.bind(this);
        this.onSelect = props.onSelect.bind(this);
        this.deleteSavedSearch = props.deleteSavedSearch;
        this.runSavedSearch = props.runSavedSearch;
    }

    render() {
    	var columns = ['Search Term', 'Filters'];
      var self = this;
      var CheckedValues = this.props.CheckedValues;
      var rows = this.props.rows;
      if(CheckedValues.length>0){
        for(var i=0;i<rows.length;i++){
          for(var j=0;j<CheckedValues.length;j++){
            if(rows[i].id==CheckedValues[j].id){
              rows[i].isChecked = CheckedValues[j].checked;
            }
          }
        }
      }
      let body = (
        <div className="table">
                <div className="col-md-10">
                <TableComponent className="jobScheduleTable"
                  columns={columns} 
                  rows={rows}
                  enableDelete={this.props.enableDelete}
                  enableSearch={this.props.enableSearch}
                   checkboxOnchange={this.props.handleChange}/>
                   

                </div>
                 <Paginate pageDetails={this.props.pageDetails} showDetail = {true}  onSelect={this.props.onSelect} />
                   <div className= "pe-btn-bar savedSearchBar">
                        <button className="pe-btn runSearchBtn" disabled={!this.props.enableSearch} 
                        onClick={this.props.runSavedSearch}>Run Search Again</button>
                        <button className="pe-btn deleteSearch" disabled={!this.props.enableDelete} 
                        onClick={this.props.deleteSavedSearch}>Delete Search</button>
                </div>
        </div>
        )
            let empty = < div className = "alert alert-info" > No results found < /div>;

      return (
          <div>
              { this.props.rows.length > 0 ? body : empty }
            </div>
      );
    }
}

module.exports = SavedSearchComponent;



