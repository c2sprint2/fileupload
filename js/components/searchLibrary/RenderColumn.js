import React from 'react';

class RenderColumn extends React.Component{

render(){
	var column = this.props.cols.map(function(column,index){
		return <div className='col-14' key={index}><b><u>{column}</u></b></div>;
	});	

	return(
		<div className="row">
			{column}
		</div>
		)
}
}

export default RenderColumn;