import React from 'react';
import Row from './RenderRow.js';
import Column from './RenderColumn.js';

class tableComponent extends React.Component {

  constructor(props){
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }

  handleChange(obj){
    if(this.props.checkboxOnchange){
            this.props.checkboxOnchange(obj);
    }
  }
  render(){
    var column = this.props.columns.map(function(column,index){
    return column;
  }); 
    return(
      <div>
        <Column cols={this.props.columns}/>
        <Row rows={this.props.rows} CheckboxHandler={this.handleChange}/>
        </div>
     
      );
  }
}

module.exports= tableComponent;