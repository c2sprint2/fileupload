/**
*
*/
import React from 'react';

class Checkbox extends React.Component{

constructor(props) {
    super(props);

    this.assetSelectedEvent = props.parent.bind(this);
    this.customFn = props.customFn.bind(this);
 }

static propTypes= {
    id:React.PropTypes.string,
    value: React.PropTypes.bool,
    name: React.PropTypes.string,
    disabled: React.PropTypes.bool,
    required: React.PropTypes.bool,
    maxLength:React.PropTypes.string,
    autofocus: React.PropTypes.bool,
    parent : React.PropTypes.func,

}
static defaultProps ={
      id:'',
      value: true,
      name: '',
      disabled:false,
      required:false,
      maxLength:'30',
      autofocus: false,
      record: {}
}


render() {

    const inputState = (value) => {
      if (this.props.value.touched && this.props.value.error) {
        return 'pe-input--error'
      } else if (this.props.value.touched && this.props.value.value) {
        return ''
      } else {
        return ''
      }
    }

        return (
            <input
            id={this.props.id}
            className= {this.props.className +' '+ inputState(this.props.value)}
              ref="input" type="checkbox"
              name={this.props.name}
              record = {this.props.record}
               required={this.props.required}
               onClick={this.assetSelectedEvent}
               maxLength={this.props.maxLength}
                value ={this.props.value} {...this.props.value}
                placeholder={this.props.placeholder}
                readOnly={this.props.readOnly}
                 disabled={this.props.disabled}
            />
        )
    }

};

module.exports = Checkbox;
