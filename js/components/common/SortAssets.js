import React from 'react';
import ReactDOM from 'react-dom';

class SortAssets extends React.Component{

	constructor(){
		super();
		this.state={
			value:''
		}
	}

	change(event){
         this.setState({value: event.target.value});
         this.props.change(event.target.value);
     }

     assignKeys(sortValue){ 
     	if(sortValue == 'Date Uploaded (Descending)'){ 
     		return 'cm:created|true';
     	}else if(sortValue == 'Date Uploaded (Ascending)'){
     		return 'cm:created|false'
     	}else if(sortValue == 'Name Ascending A-Z'){
     		return 'cm:name|true';
     	}else if(sortValue == 'Name Descending Z-A'){
     		return 'cm:name|false';
     	}

     }

	render(){ 
		let self = this;
		let sorting = new Map();
		let sortOption = this.props.sortOptions;
		let options = sortOption.map(function(sortData, index){
					  sorting.set(sortData, self.assignKeys(sortData));
                  	  return <option key={sortData} value={sorting.get(sortData)}>{sortData}</option>
                  	});

        let sortView = <select id="sort" className='itemPerPageSelectBox' onChange={this.change.bind(this)} value={this.state.value}>
        				
        				{options}</select>
		
		return(
				<div className="sort-asset">
						{sortView}
				</div>

		)
	}

}
SortAssets.propTypes={
 change:React.PropTypes.func,
};

module.exports= SortAssets;