import React from 'react';

class PE_Tab extends React.Component {
    render(){
        return (
            <div>
                {this.props.children}
            </div>
        )
    }
}

export default PE_Tab