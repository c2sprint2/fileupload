import { combineReducers } from 'redux'
import fileUpload from './fileUpload'
import {reducer as formReducer} from 'redux-form';
import CheckJobStatusReducers from './CheckJobStatusReducers';
import assets from './assets';
import searchAssets from './searchAssets';
import quad from './quad';
import { routerReducer } from 'react-router-redux'
import TreePaneReducers from './TreePaneReducer';
import autoCompleteReducer from './autoCompleteReducer';
import searchLibraryReducer from './searchLibraryReducer';
import savedSearchReducers from './savedSearchReducers';
import SingleFileFolderReducer from './SingleFileFolderReducer';

const rootMetaData = combineReducers({
	form: formReducer,
	routing: routerReducer,
	CheckJobStatusReducers,
    assets,
    searchAssets,
    quad,
	fileUpload,
	TreePaneReducers,
	SingleFileFolderReducer,
	autoComplete:autoCompleteReducer,
	searchLibraryReducer,
	savedSearchReducers
});

export default rootMetaData
