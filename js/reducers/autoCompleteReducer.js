import {AUTO_COMPLETE} from '../constants/searchLibraryConstants'
var initilizeData = [{

  }]

const autoCompleteReducer = (state = initilizeData, action) => {
  switch (action.type) {
    case AUTO_COMPLETE:
      return[ 
        ...state, {
            data:action.data,
            text: action.text,
            savedSearch:action.savedSearch,
            lastThreeSearch:action.lastThreeSearch
        }
      ]
    default:
      return state
  }
}

module.exports= autoCompleteReducer;
