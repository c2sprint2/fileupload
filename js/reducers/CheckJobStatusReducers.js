const initilizeData = [{
      rows : [],
      columns : []
      
}];

const CheckJobStatusReducers = (state = initilizeData, action) => { 
  
  switch (action.type) {
    case 'JOB_STATUS':
      return[
        ...state, action.data
      ]
    case 'JOB_STATUS_UPDATE':
    	
    return [
        ...initilizeData, action.data
      ];
  
    case 'JOB_STATUS_END':
    
    return [
        ...initilizeData, action.data
      ];

    default:
      return state
  }
}

module.exports= CheckJobStatusReducers;
